(async()=>{
	"use strict"

	const mongo = require( './mongo.js' );

	// create connect client
	let dbClient = await mongo.connect("localhost", 27017);

	if( !dbClient ){
		throw new Error('db connect is fail...');
	}

	let txnHash = "txnHash1";
	let nodeId = "def";

	// get dbInst and collection
	let col = await mongo.initDB(dbClient, "testDB").collection("testCollection");

	// process
	let result = await col.findOneAndUpdate({txnHash:txnHash},  { $addToSet: {nodes: nodeId} }, { upsert: true });

	let cursor = await col.find({txnHash:txnHash});
	cursor.forEach((v)=>{
		console.log(v);
	});


	// release client
	mongo.releaseDB(dbClient);
})();