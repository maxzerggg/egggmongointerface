(()=>{
	"use strict";

	const {MongoClient:client, ObjectID} = require( 'mongodb' );

	module.exports = {
		connect: async (host, port)=>{
			return await client.connect(`mongodb://${host}:${port}`, { useNewUrlParser: true });
		},
		initDB: (dbClient, dbName)=>{
			try{
				return dbClient.db(`${dbName}`);
			}catch(err){
				throw(err);
			}
		},
		releaseDB:(dbClient)=>{
			if ( dbClient ) {
				return dbClient.close();
			}
			throw("dbClient should not be null");
		},
		createObjectID:()=>{
			return new ObjectID();
		},
		ObjectID:ObjectID
	}
})();
